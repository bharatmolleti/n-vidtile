package com.example.nvidtile;

// see, i have an idea, 
// lets do this.. first show up a single video, 
// if the user clicks on screen, show up 2,
// if the user clicks again, show up 3.
// later,  first draw up 4 video views and play them, for step 1.

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

	final String TAG = "Test";
	
	private static final String Path = Environment.getExternalStorageDirectory() + "/Videos/";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int width = size.x;
		int height = size.y;

		Log.d(TAG, "Obtained width : " + width + ", height : " + height);

		setContentView(R.layout.activity_main);

		SurfaceView videoView1 = (SurfaceView) findViewById(R.id.viewVideo1);
		android.view.ViewGroup.LayoutParams params = videoView1
				.getLayoutParams();
		params.width = width / 2;
		params.height = height / 2;
		videoView1.setLayoutParams(params);

		videoView1.getHolder().addCallback(new SurfaceCallBack(Path + "test1080p1.mp4"));

		
		SurfaceView videoView2 = (SurfaceView) findViewById(R.id.viewVideo2);
		android.view.ViewGroup.LayoutParams params2 = videoView2
				.getLayoutParams();
		params2.width = width / 2;
		params2.height = height / 2;
		videoView2.setLayoutParams(params2);
		videoView2.getHolder().addCallback(new SurfaceCallBack(Path + "test1080p2.mp4"));

		
		SurfaceView videoView3 = (SurfaceView) findViewById(R.id.viewVideo3);
		android.view.ViewGroup.LayoutParams params3 = videoView3
				.getLayoutParams();
		params3.width = width / 2;
		params3.height = height / 2;
		videoView3.setLayoutParams(params3);
		videoView3.getHolder().addCallback(new SurfaceCallBack(Path + "test1080p3.mp4"));

		
		SurfaceView videoView4 = (SurfaceView) findViewById(R.id.viewVideo4);
		android.view.ViewGroup.LayoutParams params4 = videoView4
				.getLayoutParams();
		params4.width = width / 2;
		params4.height = height / 2;
		videoView4.setLayoutParams(params4);
		videoView4.getHolder().addCallback(new SurfaceCallBack(Path + "test1080p4.mp4"));

	}
}
