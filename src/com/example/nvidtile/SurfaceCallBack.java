package com.example.nvidtile;

import android.view.SurfaceHolder;

public class SurfaceCallBack implements SurfaceHolder.Callback{
	
	private PlayerThread mPlayer = null;
	
	private String sFilename = null;
	
	public SurfaceCallBack(String file){
		sFilename = file;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int arg1, int arg2, int arg3) {
		if (mPlayer == null) {
            mPlayer = new PlayerThread(holder.getSurface(), sFilename);
            mPlayer.start();
        }		
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		if (mPlayer != null) {
            mPlayer.interrupt();
        }		
	}
}
